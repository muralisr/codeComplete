var pg = require('pg')
var connectionString = '<yourconnectionstring>'
var express = require('express')
var ASTVector = require('../public/javascripts/ASTVector.js')
var router = express.Router()
var parseDammit = require('acorn/dist/acorn_loose').parse_dammit
var walk = require('esprima-walk')
require('string_score')
var content = ''
/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index')
});

router.post('/ast', function (req, res, next) {
  content = req.body.code
  createAst(content, function (err, array) {
    if (err) {
      res.send('No match for the given completion')
    } else {
      getMatchingVectorCode(array, function (err, results) {        
        if (err) {
          res.send('No match for the given completion')
        }
        var arr = results.map(function (item) {
          return item['code'].concat('\n //Code Source: ' + item['source'])
        });        
        arr = arr.filter(isMatch)
        arr.sort(function (a, b) {
          return b.score(content) - a.score(content)
        })
        res.send(arr.slice(0, 10))
      });
    }
  });
});

function isMatch (value) {
  return value.trim().replace(/(\r\n|\n|\r)/gm, '').indexOf(content.trim().replace(/(\r\n|\n|\r)/gm, '')) > -1
}

function createAst (content, callback) {
  var parse_options = { }
  parse_options = {
    ecmaVersion: 6
  }
  var ast = parseDammit(content, parse_options)
  var array = []
  walk(ast, function (node) {
    if (node.type === 'ClassDeclaration' || node.type === 'ClassExpression' || node.type === 'FunctionExpression' || node.type === 'FunctionDeclaration' || node.type === 'ArrowFunctionExpression') {
      if (array.length === 0) {
        array = processFunctionData(node)
      }
    }
  });
  callback(null, array)
}

function processFunctionData (node) {
  var vectorArray = new Array(65).fill(0)
  var ast = new ASTVector()
  var index = ast[node.type]
  vectorArray[index] += 1
  traverse(node.body, function (node) {
    vectorArray = checkNodeType(node, vectorArray)
  });  
  return vectorArray
}

function checkNodeType (node, vectorArray) {
  var ast = new ASTVector()
  if (typeof node.type !== 'undefined') {
    var index = ast[node.type]
    vectorArray[index] += 1
  }
  return vectorArray
}

function traverse (node, func) {
  func(node)
  for (var key in node) {
    if (node.hasOwnProperty(key)) {
      var child = node[key]
      if (typeof child === 'object' && child !== null) {
        if (Array.isArray(child)) {
          child.forEach(function (node) {
            traverse(node, func)
          });
        } else {
          traverse(child, func)
        }
      }
    }
  }
}

function getMatchingVectorCode (vector, callback) {
  pg.connect(connectionString, function (err, client, done) {
        // Handle connection errors
    if (err) {
      done()
    } else {
      client.query('Select a.code as code, a.source as source from content a join autocomplete b on a.code_id=b.code_id where b.vector=$1 group by a.code,a.source', [vector], function (err, results) {
        done()
        if (err) {
          console.log(err)
        } else {
          callback(null, results.rows)
        }
      });
    }
  });
};

module.exports = router
