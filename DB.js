var pg = require('pg')
var connectionString = '<yourconnectionstring>'

exports.saveCodeContent = function (code_uuid, code, source) {
  pg.connect(connectionString, function (err, client, done) {
    // Handle connection errors
    if (err) {
      done()
      console.log(err)
    } else {
      client.query('Insert into content (code,code_id,source) values($1,$2,$3)', [code, code_uuid, source], function (err, results) {
        done()
        if (err) {
          console.log(err)
        }
      });
    }
  });
};


exports.saveVectors = function (vector, code_uuid) {
  pg.connect(connectionString, function (err, client, done) {
        // Handle connection errors
    if (err) {
      done()
    } else {
      client.query('Insert into autocomplete (vector,code_id) values($1,$2)', [vector, code_uuid], function (err, results) {
        done()
        if (err) {
          console.log(err)                    
        }
      });
    }
  });
};
