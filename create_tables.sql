Create table autocomplete(
	id serial primary key,
	code_id varchar(100),
	vector INTEGER ARRAY[65]	
);

Create table content(
	id serial primary key,
	code_id varchar(50),
	code text
);