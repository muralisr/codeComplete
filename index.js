var acorn = require("acorn")
var walk = require("esprima-walk")
var astVector = require("./ASTVector.js")
fs = require("fs")
var parseDammit = require("acorn/dist/acorn_loose").parse_dammit
require("string_score")
var db = require("./DB.js")
var uuid = require('node-uuid')
var toSaveData = []

var walkSync = function(dir, filelist) {
    var fs = fs || require('fs'),
        files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function(file) {
        if (fs.statSync(dir + '/' + file).isDirectory()) {
            filelist = walkSync(dir + '/' + file, filelist);
        }
        else {            
            filelist.push(dir + '/' + file);
        }
    });
    return filelist;
};

var fileList = walkSync(process.argv[2]);
var source=process.argv[3];

fileList.forEach(function(file) {
    var fullName = file;    
    try {
        fs.readFile(fullName, 'utf8', function(err, data) {
            if (err) {
                return console.log(err);
            }
            createAst(data, fullName,source);
        });
    }
    catch (e) {
        console.log("Error when parsing file ", fullName);
    }
});


function createAst(content, fileName,source) {
    parse_options = {
        ecmaVersion: 6,
        locations: true
    }
    try {
        ast = acorn.parse(content, parse_options);        
        walk(ast, function(node) {                           
            if (node.type === 'FunctionExpression' || node.type === 'FunctionDeclaration' || node.type === 'ArrowFunctionExpression' || node.type === 'ClassDeclaration' || node.type === 'ClassExpression' ) {                
                processFunctionData(node, fileName);                
            }           
        });                
        saveToDB(source);
    } catch (e) {
        console.log("Exception in processing ", fileName);
    }
}
function saveToDB(source) {    
    for (i = 0; i < toSaveData.length; i++) {
        var currentVectors = toSaveData[i].vectors;
        var currentCode = toSaveData[i].code;
        var code_uuid = uuid.v4();        
        db.saveCodeContent(code_uuid, currentCode,source);        
        for (j = 0; j < currentVectors.length; j++) {
            db.saveVectors(currentVectors[j], code_uuid);
        }
    }        
    toSaveData=[];
}

function processFunctionData(node, fileName) {
    var contentOfFunction = readFileFunctionFromLine(node.loc.start.line, node.loc.end.line, fileName);    
    var contentData = new Object();
    var eachStageVector = new Array();

    var vectorArray = new Array(65).fill(0);
    var ast = new astVector();
    var index = ast[node.type];
    vectorArray[index] += 1;
    traverse(node.body, function(node) {
        vectorArray = checkNodeType(node, vectorArray);
        eachStageVector.push(vectorArray.slice());
    });
        
    contentData.vectors = eachStageVector;

    contentData.code = contentOfFunction.toString('ascii');    
    toSaveData.push(contentData);
}

function readFileFunctionFromLine(start, end, fileName) {
    var arguments = 'sed -n \'' + start + ',' + end + 'p\' ' + fileName;
    const exec = require('child_process').execSync;
    var func = exec(arguments);
    return func;
}


function checkNodeType(node, vectorArray) {
    var ast = new astVector();
    if (typeof node.type != 'undefined') {
        var index = ast[node.type];
        vectorArray[index] += 1;        
    }
    return vectorArray;
}

//recursively search if its a function, class, functionexpression, any logical unit together
function traverse(node, func) {
    func(node);//1
    for (var key in node) { //2
        if (node.hasOwnProperty(key)) { //3
            var child = node[key];
            if (typeof child === 'object' && child !== null) { //4

                if (Array.isArray(child)) {
                    child.forEach(function(node) { //5
                        traverse(node, func);
                    });
                } else {
                    traverse(child, func); //6
                }
            }
        }
    }
}
