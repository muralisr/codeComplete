# Open source Auto-Complete 

Plugin to auto-complete code from JavaScript open source projects.

# Installation

To run the web-based editor locally:

#### To gather knowledge base

* Install [postgresql] www.postgresql.org/download/

* `npm install`

* Replace `yourconnectionstring` in index.js with your Database connection string

* `node index.js '/path_to_open_source_project' 'source_name'


#### To run the editor

* cd into autocomplete

* `npm install`

* Replace `yourconnectionstring` in index.js with your Database connection string

* `npm run start`

# Components


* **Vectors** : 
    The project uses [acorn] https://github.com/ternjs/acorn to convert source code to AST.
    The AST is walked and logical blocks like function declaration/expression, arrow function, class declaration/expression are converted to vectors.

* **Code Completion Search** : 
    In the web-based editor as the user searches for a completion, partial AST vector is generated for the user code and is searched from the database and string score is used to rank the results to add more context and relevance
 



#### Copyright (c) 2016 Murali Ramanujam.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
